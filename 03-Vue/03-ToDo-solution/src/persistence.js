import axios from "axios";

const API_URL = 'http://localhost:3456/todos';

export function loadToDos(completed) {
  let url = API_URL;
  if (completed === true){
    url += '?completed=1'
  }
  else if (completed === false){
    url += '?completed=0'
  }

  return axios.get(url)
    .then(response => {
      return response.data.data;
    })
    .catch((error) => console.log(error));
}

export function saveToDo(todo) {
  return axios.post(API_URL, todo)
    .then(response => {
      return response.data.data;
    })
    .catch((error) => console.log(error));
}

export function updateToDo(todo) {

  return axios.put(`${API_URL}/${todo.id}`, todo)
    .catch((error) => console.log(error));
}

export function deleteToDo(todo) {
  return axios.delete(`${API_URL}/${todo.id}`)
    .catch((error) => console.log(error));
}
