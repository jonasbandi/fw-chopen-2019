import Vue from 'vue'
import Router from 'vue-router'
import ToDoScreen from './components/ToDoScreen.vue'
import DoneScreen from './components/DoneScreen.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: ToDoScreen
    },
    {
      path: '/done',
      name: 'done',
      component: DoneScreen
    },
  ]
})
