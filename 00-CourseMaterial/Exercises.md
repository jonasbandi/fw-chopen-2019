# Angular

## Basic Exercise

Create a new Angular application with the Anguler CLI:

```
ng new ng-app --routing
```

Create a new component with the Angular CLI:

```
ng generate component greeter
```

- Extend the `app` component so that this new `greeter` component is used/displayed.
- Change the `greeter` component: It should have an input where you can type your name and below the input a "Greeting" with your name should be displayed.



## Advanced Exercise

Start the backend server. See instructions below.

Go into the directory `01-Angular/02-ToDo-exercise` and run:

```
npm install
npm start
```

- Study the code of the application (components, routing).
- Extract a `NewToDo` component
- Extend the functionality so that "add" & "complete" is persisted.



# React

Create a new Vue application with the Vue CLI:

```
create-react-app react-app
```

- Create a new `greeter` component 

- Extend the `app` component so that this new `greeter` component is used/displayed.

- Change the `greeter` component: It should have an input where you can type your name and below the input a "Greeting" with your name should be displayed.

- Use `react-router` and configure routing so that the `greeter` is shown on the default route.

- Add a second route that displays a `goobye` component.

  

## Advanced Exercise

Start the backend server. See instructions below.

Go into the directory `02-React/02-ToDo-exercise` and run:

```
npm install
npm start
```

- Study the code of the application (components, routing).
- Extract a `NewToDo` component
- Extend the functionality so that "add" & "complete" is persisted.







# Vue.js

## Vue.js for DOM Enhancement



## Vue.js for SPA

Create a new Vue application with the Vue CLI:
```
vue create vue-app
```

- Create a new `greeter` component 
- Extend the `app` component so that this new `greeter` component is used/displayed.
- Change the `greeter` component: It should have an input where you can type your name and below the input a "Greeting" with your name should be displayed.



## Advanced Exercise

Start the backend server. See instructions below.

Go into the directory `01-Vue/02-Advanced` and run:

```
npm install
npm start
```

- Study the code of the application (components, routing).
- Extract a `NewToDo` component
- Extend the functionality so that "add" & "complete" is persisted.





# Backend Server

The directory `_server` contains a simple API-Server implementing basic CRUD functionality for our ToDo application.
Start the server with the following commands:

```
npm install #just once
npm start
```

You should now get an array with two todo items at the url: `http://localhost:3456/todos`.

The API implemented by the REST-Endpoint is described in the table below:

| HTTP-Method | URL (example)                                                | Request-Body                            | Response                    |
| ----------- | ------------------------------------------------------------ | --------------------------------------- | --------------------------- |
| GET         | http://localhost:3456/todos   *(optional query-parameter: ?completed=0 or 1)* |                                         | {data: [{*todo*},{*todo*}]} |
| GET         | http://localhost:3456/todos/1                                |                                         | {data: {*todo*} }           |
| POST        | http://localhost:3456/todos                                  | { "title": "Test", "completed": false}  | {data: {*todo*} }           |
| PUT         | http://localhost:3456/todos/1                                | { "title": "Test 2", "completed": true} | *empty*                     |
| DELETE      | http://localhost:3456/todos/1                                |                                         | *empty*                     |

Note that all responses are wrapped in a response object with a `data` property.
This is a typical security measure of JSON endpoints. See: http://stackoverflow.com/questions/3503102/what-are-top-level-json-arrays-and-why-are-they-a-security-risk