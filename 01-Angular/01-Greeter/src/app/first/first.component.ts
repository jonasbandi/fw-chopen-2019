import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './first.component.html',
})
export class FirstComponent implements OnInit {

  messageFromParent = 'Hello';

  constructor() { }

  ngOnInit() {
  }

}
