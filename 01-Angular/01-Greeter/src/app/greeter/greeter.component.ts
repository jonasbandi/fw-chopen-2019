import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-greeter',
  template: `
      <h1>{{message}} {{name}}</h1>
      <input [(ngModel)]="name">
  `
})
export class GreeterComponent implements OnInit {

  @Input() message;
  name = '';

  constructor() {
  }

  ngOnInit() {
  }

}
