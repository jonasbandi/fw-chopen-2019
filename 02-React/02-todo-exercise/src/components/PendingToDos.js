import React, {useState, useEffect} from 'react';
import NewToDoForm from './NewToDoForm';
import ToDoList from './ToDoList';
import {loadToDos, saveToDo, updateToDo} from '../persistence';

function PendingToDos() {

  const [toDoTitle, setToDoTitle] = useState('');
  const [todos, setTodos] = useState([]);
  const [message, setMessage] = useState('');

  useEffect(() => {
    // using an IIFE since the effect function is not allowed to return a value (exept the cleanup function)
    // and async functions implicitly return a promise
    (async () => {
      setMessage('Loading ...');
      const todos = await loadToDos();
      setTodos(todos);
      setMessage('');
    })();
  }, []);

  function formChange(e) {
    setToDoTitle(e.currentTarget.value);
  }

  async function addToDo(e) {
    e.preventDefault();
    const newToDo = {title: toDoTitle, completed: false};

    // "OPTIMISTIC UI"
    const newToDos = [...todos, newToDo];
    setTodos(newToDos);

    // TODO: persist - Part of the exercise
    setToDoTitle('');
  }

  async function completeToDo(toDo) {
    toDo.completed = true;

    // "OPTIMISTIC UI"
    const updatedToDos = todos.filter(t => t.id !== toDo.id);

    // TODO: persist - part of the exercise
  }

  let loadingIndicator =
    message
      ? <div>{message}</div>
      : null;

  return (
    <div>
      {loadingIndicator}

      {/*TODO: exercise - extract form component */}
      <form className="new-todo" onSubmit={addToDo}>
        <input id="todo-text" name="toDoTitle" type="text" placeholder="What needs to be done?"
               autoFocus
               autoComplete="off"
               value={toDoTitle}
               onChange={formChange}
        />

        <button id="add-button" className="add-button">+</button>
      </form>

      <div className="main">
        <ToDoList todos={todos} onRemoveToDo={completeToDo}/>
      </div>
    </div>
  );
}

export default PendingToDos;
