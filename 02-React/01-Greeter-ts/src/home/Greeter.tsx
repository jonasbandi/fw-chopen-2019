import React, { useState } from 'react';

export function Greeter({message}: { message: string }) {

    const [name, setName] = useState('');

    return (
        <div>
            <h1>{message} {name}</h1>
            <input value={name} onChange={(e) => setName(e.target.value)}/>
        </div>
    )
}
