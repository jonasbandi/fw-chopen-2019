import React from 'react';
import styles from './SecondScreen.module.scss';

function SecondScreen() {
  return (
    <div className={styles.comic}>
      <h1>Have a nice day!</h1>
    </div>
  );
}

export default SecondScreen;
