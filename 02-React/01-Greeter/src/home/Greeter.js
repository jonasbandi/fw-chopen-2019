import React, {useState} from 'react';

export function Greeter(props) {

  const [name, setName] = useState('');

  return (
    <div >
      <h1>{props.message} {name}</h1>
      <input value={name} onChange={(e) => setName(e.target.value)}/>
    </div>
  );
}

export default Greeter;
